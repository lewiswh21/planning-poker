import React, { useRef, useState } from "react"
import { Form, Button, Card, Alert, Row, Col } from "react-bootstrap"
import { useAuth } from "../contexts/AuthContext"
import { Link, useHistory } from "react-router-dom"
import avatarIcons from "../data/avatars.json"

export default function Signup() {
  const emailRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()
  const displayNameRef = useRef()
  const { signup } = useAuth()
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const [selectedAvatar, setSelectedAvatar] = useState("avatar-1")
  const history = useHistory()

  async function handleSubmit(e) {
    e.preventDefault()
    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match")
    }
    try {
      setError("")
      setLoading(true)
      await signup(emailRef.current.value, passwordRef.current.value, displayNameRef.current.value, selectedAvatar)
      history.push("/")
    } catch {
      setError("An account with this email already exists")
      if (passwordRef.current.value.length < 6) {
        setError("Password must be at least 6 characters")
      }
    }
    setLoading(false)
  }

  return (
    <div className="signupForm text-center">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 mt-2 authFormTitle">Create A New Account</h2>
          {error && <Alert className="authFormErrorAlert" variant="danger">{error}</Alert>}
          <Form onSubmit={handleSubmit}>
            <Row className='leftAlign signup-form-row'>
              <Col>
                <Form.Group id="email" className="formInput">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" ref={emailRef} required />
                </Form.Group>
                <Form.Group id="displayName" className="formInput">
                  <Form.Label>Display Name</Form.Label>
                  <Form.Control type="text" ref={displayNameRef} required />
                </Form.Group>
                <Form.Group id="password" className="formInput">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" ref={passwordRef} placeholder="Passwords must be at least 6 characters" autoComplete="on" required />
                </Form.Group>
                <Form.Group id="password-confirm" className="formInput">
                  <Form.Label>Password Confirmation</Form.Label>
                  <Form.Control type="password" ref={passwordConfirmRef} placeholder="Passwords must be at least 6 characters" autoComplete="on" required />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group id="avatar" className="formInput">
                  <Form.Label>Select An Avatar</Form.Label>
                  <Card className='flexAlign avatarsContainer'>
                    {
                      avatarIcons.avatars.map((avatar, index) => (
                        <React.Fragment key={index}>
                          <input className='hideMe' type="radio" id={avatar.avatarUrl} name="avatar" value={avatar.avatarUrl} onChange={(e) => setSelectedAvatar(e.target.value)} />
                          <label className='avatarSelectionLabel' htmlFor={avatar.avatarUrl}>
                            <img className='avatarSelection' src={require(`../images/avatars/${avatar.avatarUrl}.png`)} alt="avatar"></img>
                          </label>
                        </React.Fragment>
                      ))
                    }
                  </Card>
                </Form.Group>
              </Col>
            </Row>
            <Button disabled={loading} className="formButton mb-2" type="submit"> Sign Up </Button>
          </Form>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        Already have an account? <Link to="/login">Log In</Link>
      </div>
    </div>
  )
}