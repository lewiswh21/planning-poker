import React, { useEffect } from "react"
import { CardGroup } from "react-bootstrap"
import CreateGame from "../components/gameAreas/CreateGame"
import JoinGame from "../components/gameAreas/JoinGame"
import { dBClean } from "../helpers/firebaseHelpers"

export default function Dashboard() {

  //CHECK/RUN LAST DB CLEAN ONCE ON MOUNT
  useEffect(() => {
    dBClean()
  }, [])

  return (
    <div className="gameFormContainer">
      <CardGroup>
        <CreateGame />
        <JoinGame />
      </CardGroup>
    </div>
  )
}