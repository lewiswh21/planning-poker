import React, { useEffect, useRef, useState } from "react"
import { Form, Button, Card, Alert, Row, Col } from "react-bootstrap"
import { useAuth } from "../contexts/AuthContext"
import { Link, useHistory } from "react-router-dom"
import avatarIcons from "../data/avatars.json"

export default function UpdateProfile() {
  const emailRef = useRef()
  const displayNameRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()
  const { currentUser, updatePassword, updateEmail, updateDetails } = useAuth()
  const [selectedAvatar, setSelectedAvatar] = useState(currentUser.photoURL)
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const history = useHistory()

  useEffect(() => {
    document.getElementById(selectedAvatar).checked = true
  }, [selectedAvatar])

  function handleSubmit(e) {
    e.preventDefault()
    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match")
    }

    const promises = []
    setLoading(true)
    setError("")

    if (emailRef.current.value !== currentUser.email) {
      promises.push(updateEmail(emailRef.current.value))
    }
    if (displayNameRef.current.value || !currentUser.photoURL === selectedAvatar) {
      promises.push(updateDetails(displayNameRef.current.value, selectedAvatar))
    }
    if (passwordRef.current.value) {
      promises.push(updatePassword(passwordRef.current.value))
    }

    Promise.all(promises)
      .then(() => {
        history.push("/")
      })
      .catch(() => {
        setError("Failed to update account")
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <div className="signupForm text-center">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 mt-2 authFormTitle">Update Profile</h2>
          {error && <Alert className="authFormErrorAlert" variant="danger">{error}</Alert>}
          <Form onSubmit={handleSubmit}>
            <Row className='leftAlign signup-form-row'>
              <Col>
                <Form.Group id="email" className="formInput">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" ref={emailRef} required defaultValue={currentUser.email} />
                </Form.Group>
                <Form.Group id="displayName" className="formInput">
                  <Form.Label>Display Name</Form.Label>
                  <Form.Control type="text" ref={displayNameRef} required defaultValue={currentUser.displayName} />
                </Form.Group>
                <Form.Group id="password" className="formInput">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" ref={passwordRef} placeholder="Leave blank to keep the same" autoComplete="on" />
                </Form.Group>
                <Form.Group id="password-confirm" className="formInput">
                  <Form.Label>Password Confirmation</Form.Label>
                  <Form.Control type="password" ref={passwordConfirmRef} placeholder="Leave blank to keep the same" autoComplete="on" />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group id="avatar" className="formInput">
                  <Form.Label>Select An Avatar</Form.Label>
                  <Card className='flexAlign avatarsContainer'>
                    {
                      avatarIcons.avatars.map((avatar, index) => (
                        <React.Fragment key={index}>
                          <input className='hideMe' type="radio" id={avatar.avatarUrl} name="avatar" value={avatar.avatarUrl} onChange={(e) => setSelectedAvatar(e.target.value)} />
                          <label className='avatarSelectionLabel' htmlFor={avatar.avatarUrl}>
                            <img className='avatarSelection' src={require(`../images/avatars/${avatar.avatarUrl}.png`)} alt="avatar"></img>
                          </label>
                        </React.Fragment>
                      ))
                    }
                  </Card>
                </Form.Group>
              </Col>
            </Row>
            <Button disabled={loading} className="formButton mb-2" type="submit"> Update </Button>
          </Form>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        <Link to="/">Cancel</Link>
      </div>
    </div>
  )
}