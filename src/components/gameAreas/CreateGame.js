import { useState, useContext } from 'react'
import UserContext from '../../contexts/UserContext'
import { writeUserData, writeRoomData, writeCurrentUsers } from '../../helpers/firebaseHelpers'
import { generateUniqueID, getCurrentTimestamp } from '../../helpers/gameHelpers'
import { useHistory } from 'react-router-dom'
import { useAuth } from '../../contexts/AuthContext'
import { Form, Button, Card, Dropdown } from "react-bootstrap"

export default function CreateGame() {
    const { currentUser } = useAuth()
    const [roomName, setRoomName] = useState('')
    const [gameType, setGameType] = useState('fib')
    const { setUser } = useContext(UserContext)
    const history = useHistory()

    const createGame = (e) => {
        e.preventDefault()
        let userData = { username: currentUser.displayName, gameID: generateUniqueID(), playerID: currentUser.uid, roomName, gameType, playerState: "hosting", lastGame: getCurrentTimestamp(), avatar: currentUser.photoURL }
        setUser(userData)
        writeUserData(userData)
        writeRoomData(userData)
        writeCurrentUsers(userData.gameID, userData, currentUser.email)
        history.push(`/game/${userData.gameID}`)
    }

    return (
        <Card className='hostFormCard'>
            <Card.Body>
                <h2 className="text-center mb-4 authFormTitle">Host Game</h2>
                <Form onSubmit={createGame}>
                    <Form.Group id="topic" className="formInput">
                        <Form.Control type="text" value={roomName} required placeholder="Enter A Topic" onChange={(e) => setRoomName(e.target.value)} />
                    </Form.Group>
                    <Dropdown className='game-type-dropdown'>
                        <Dropdown.Toggle className='game-type-dropdown-toggle btn-secondary'>
                            {(gameType && gameType === "fib") && <div className='game-type-dropdown-text'>Fibonacci Sequence</div>}
                            {(gameType && gameType === "tee") && <div className='game-type-dropdown-text'>T-Shirt Sizing</div>}
                        </Dropdown.Toggle>
                        <Dropdown.Menu className='game-type-dropdown-menu'>
                            <Dropdown.Item onClick={() => setGameType("fib")}>Fibonacci Sequence</Dropdown.Item>
                            <Dropdown.Item onClick={() => setGameType("tee")}>T-Shirt Sizing</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <Button className="w-100 formButton" type="submit">
                        Create Game
                    </Button>
                </Form>
            </Card.Body>
            <br />
        </Card>
    )
}
