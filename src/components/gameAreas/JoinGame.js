import { useEffect, useState } from 'react'
import { useAuth } from '../../contexts/AuthContext'
import { Form, Button, Card, Alert } from "react-bootstrap"
import { checkRoomExists } from "../../helpers/firebaseHelpers"
import { useHistory } from 'react-router-dom'

export default function JoinGame() {
    const { currentUser } = useAuth()
    const [gameID, setGameID] = useState('')
    const [gameExists, setGameExists] = useState(null)
    const [error, setError] = useState('')
    const history = useHistory()

    useEffect(() => {
        if (gameExists === true) {
            history.push(`/game/${gameID}`)
        } 
        if (gameExists === false) {
            setError("This GameID is invalid")
        } 
    }, [gameExists]) // eslint-disable-line react-hooks/exhaustive-deps

    const joinGame = (e) => {
        e.preventDefault()
        setGameExists(checkRoomExists(gameID, setGameExists))
    }

    return (
        <Card className='joinFormCard'>
            <Card.Body>
                <h2 className="text-center mb-4 authFormTitle">Join Game</h2>
                {error && <Alert className="authFormErrorAlert" variant="danger">{error}</Alert>}
                <Form onSubmit={joinGame}>
                    <Form.Group id="email" className="formInput">
                        <Form.Control type="email" value={currentUser.email} disabled placeholder={currentUser.email} />
                    </Form.Group>
                    <Form.Group id="gameID" className="formInput">
                        <Form.Control type="text" value={gameID} required placeholder="Game ID (18 characters)" onChange={(e) => setGameID(e.target.value)} />
                    </Form.Group>
                    <Button className="w-100 formButton" type="submit">
                        Join Game
                    </Button>
                </Form>
            </Card.Body>
            <br />
        </Card>
    )
}