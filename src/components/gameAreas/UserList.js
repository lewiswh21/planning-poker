import React, { useContext, useEffect, useState } from 'react'
import { Card, ListGroup, Button, Stack, Col, Row } from 'react-bootstrap'
import { useAuth } from '../../contexts/AuthContext'
import RoomContext from '../../contexts/RoomContext'
import { readCurrentUsers, removeCurrentUser } from '../../helpers/firebaseHelpers'
import { BsStickies } from 'react-icons/bs'
import { copyButtonClicked } from '../../helpers/gameHelpers'
import ReactTooltip from 'react-tooltip'

export default function UserList() {
  const [userList, setUserList] = useState([])
  const [userCount, setUserCount] = useState()
  const { currentUser } = useAuth()
  const { room } = useContext(RoomContext)

  // GET LIST OF USERS IN CURRENT ROOM ON MOUNT/RERENDER AND CHANGE OF USERLIST IN DB (ONVALUE)
  useEffect(() => {
    let isMounted = true
    if (isMounted) readCurrentUsers(room.gameID, setUserList)
    return () => { isMounted = false }
  }, [room.gameID, setUserList])

  // SET USERS LEFT/WAITING TO VOTE
  useEffect(() => {
    setUserCount(Object.keys(userList).length)
  }, [setUserCount, userList])

  // KICK USER FROM ROOM
  function kickUser(gameID, uid) {
    removeCurrentUser(gameID, uid)
  }

  return (
    <Card className="usersColumn">
      <Card.Header className="text-center">Current Users ({userCount})</Card.Header>
      <Card.Body className="usersColumnChild">
        <ListGroup>
          <div className="users">
            {userList.map((user) => (
              <ListGroup.Item className='userListGroupItem' key={user.playerID} variant="light">
                <Row>
                  <Col className='userlist-group-col-l'>
                    <img className='avatarUserlist' src={require(`../../images/avatars/${user.avatar}.png`)} alt="avatar" data-tip data-for={user.playerID}></img>
                    <ReactTooltip id={user.playerID} className="tooltip" place="left" effect="solid"> {user.email} </ReactTooltip>
                    <div className="userListUsername ellipsis">{user.username}</div>
                  </Col>
                  <Col className='userlist-group-col-r'>
                    <Stack direction="horizontal" gap={3}>
                      <div className="userListKick ms-auto ellipsis">
                        {(room.hostID === currentUser.uid && user.playerID !== currentUser.uid) &&
                          <Button className='userListBtn' variant="outline-danger" onClick={() => kickUser(user.gameID, user.playerID)}>Kick</Button>
                        }
                        {(room.hostID === user.playerID) &&
                          <Button className='userListBtn' variant="outline-success" disabled>Host</Button>
                        }
                      </div>
                    </Stack>
                    <Stack direction="horizontal" gap={3}>
                      <div className="userListState ms-auto ellipsis" data-color-code={user.playerState}>{user.playerState}</div>
                    </Stack>
                  </Col>
                </Row>
              </ListGroup.Item>
            ))}
          </div>
        </ListGroup>
      </Card.Body>
      <Card.Footer className='text-center userListFooter'>
        <Button variant="outline-primary" className='copy-url-button smallBtn' onClick={() => copyButtonClicked()}>
          <span>Copy URL</span>
          <BsStickies className='copy-url-icon' />
        </Button>
      </Card.Footer>
    </Card>
  )
}
