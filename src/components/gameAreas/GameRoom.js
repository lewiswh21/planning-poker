import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { useAuth } from "../../contexts/AuthContext"
import UserContext from "../../contexts/UserContext"
import { readUserData, checkUserValid, writeUserData, writeCurrentUsers, removeCurrentUser } from "../../helpers/firebaseHelpers"
import GameTable from "./GameTable"
import UserList from "./UserList"
import { Container, Row } from "react-bootstrap"
import { errorMessage, getCurrentTimestamp } from "../../helpers/gameHelpers.js"
import { useGetRoom } from "../../hooks/useGetRoom"

export default function GameRoom() {
    const [validUser, setValidUser] = useState(null)
    const { currentUser } = useAuth()
    const { user, setUser } = useContext(UserContext)
    const { gameID } = useParams()
    const { loading, room } = useGetRoom(gameID)

    // REMOVE USER FROM CURRENTUSERS BEFORE UNLOAD
    useEffect(() => {
        window.addEventListener("beforeunload", function (event) { removeCurrentUser(gameID, currentUser.uid) })
        return () => {
            window.removeEventListener("beforeunload", function (event) { removeCurrentUser(gameID, currentUser.uid) })
        }
    }, [gameID, currentUser.uid])

    // REMOVE USER FROM CURRENTUSERS IN ROOM ON DISMOUNT
    useEffect(() => { return () => { removeCurrentUser(gameID, currentUser.uid) } }, [gameID, currentUser.uid])

    //CHECK VALIDUSER AND SET VALIDUSER STATE
    useEffect(() => { if (!loading) { checkUserValid(currentUser.uid, gameID, setValidUser) } }, [currentUser.uid, gameID, validUser, loading])

    // IF JOINING GAME SET USER AND ROOM DATA
    useEffect(() => {
        if (!validUser) {
            let userData = { username: currentUser.displayName, gameID: gameID, playerID: currentUser.uid, roomName: "", gameType: "", playerState: "waiting", lastGame: getCurrentTimestamp(), avatar: currentUser.photoURL }
            setUser(userData)
            writeUserData(userData)
            writeCurrentUsers(gameID, userData, currentUser.email)
        }
    }, [validUser]) // eslint-disable-line react-hooks/exhaustive-deps

    //READ USERDATA FROM DB AND SET CONTEXT (dependencies would cause infinite loop)
    useEffect(() => { if (user && room.length) { readUserData(currentUser.uid, setUser) } }, []) // eslint-disable-line react-hooks/exhaustive-deps

    //SHOW LOADER WHILE LOADING OR ERROR MESSAGE IF ERROR
    if (loading) { return (<div className='loaderContainer flexCenter'> <div className="loader"></div> </div>) }
    if (!user) { return (errorMessage("REMOVED FROM ROOM")) }
    if (validUser !== null && !validUser) { return (errorMessage("INVALID USER ID")) }
    if (!room.validRoom || !room.currentUsers) { return (errorMessage("INVALID GAME ID")) }

    //SHOW GAME COMPONENTS IF ALL DATA VALID
    return (
        <div className="gameroom">
            <Container>
                <Row>
                    {!loading && room && <> <GameTable /> <UserList /> </>}
                </Row>
            </Container>
        </div>
    )
}