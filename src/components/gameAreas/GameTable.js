import React, { useContext, useEffect, useState } from 'react'
import { Card, Col, Button, Row, Alert } from 'react-bootstrap'
import { useAuth } from '../../contexts/AuthContext'
import UserContext from '../../contexts/UserContext'
import RoomContext from '../../contexts/RoomContext'
import { getRoomState, getPlayerState, removeCurrentUser, setRoomState, setPlayerState, setVote, readCurrentUsers, setRoomTimestamp } from '../../helpers/firebaseHelpers'
import { useHistory } from 'react-router-dom'
import { Chart } from "./Chart"
import ReactTooltip from 'react-tooltip'

export default function GameTable() {
  const { currentUser } = useAuth()
  const { room, setRoom } = useContext(RoomContext)
  const { user, setUser } = useContext(UserContext)
  const [userList, setUserList] = useState([])
  const [resultsList, setResultsList] = useState([])
  const [voteValues, setVoteValues] = useState([])
  const [chartData, setChartData] = useState({})
  const [votesWaiting, setVotesWaiting] = useState(0)
  const history = useHistory()

  const fibValues = ["0", "1", "2", "3", "5", "8", "13", "21", "34", "55", "88", "?"]
  const teeValues = ["XS", "S", "M", "L", "XL", "XXL"]

  //GET ROOM STATE FROM DB AND SET ROOM CONTEXT ON MOUNT/RERENDER
  useEffect(() => {
    let isMounted = true
    if (isMounted) getRoomState(room.gameID, setRoom)
    return () => { isMounted = false }
  }, [room.gameID, setRoom])

  //GET PLAYER STATE FROM DB AND SET PLAYER CONTEXT ON MOUNT/RERENDER
  useEffect(() => { getPlayerState(room.gameID, setUser, currentUser.uid) }, [room.gameID, setUser, currentUser.uid])

  // GET LIST OF USERS IN CURRENT ROOM ON MOUNT/RERENDER AND CHANGE OF USERLIST IN DB (ONVALUE)
  useEffect(() => { readCurrentUsers(room.gameID, setUserList) }, [room.gameID, setUserList])

  //SET WAITING PLAYERS AS READY AFTER CURRENT HAND ENDS
  useEffect(() => {
    if (user && room && room.gameState === "waiting" && user.playerState === "waiting") {
      setPlayerState(room.gameID, user.playerID, "ready")
    }
  }, [user.playerState, room.gameState]) // eslint-disable-line react-hooks/exhaustive-deps

  // COUNT VOTES IF ROOM STATE VOTING
  useEffect(() => { if (room.gameState === "voting" && user.playerState !== "ready") { countVotes() } })

  // USE A SEPERATE OBJECT FOR RESULTS SO IF USER LEAVES ON RESULTS PAGE THEIR SELECTION REMAINS / ADD PIE CHART
  useEffect(() => {
    if (room.gameState === "voted" && userList.length > 0) {
      const count = {}
      for (const element of userList) {
        if (element.vote !== undefined && element.vote !== "voting") {
          if (count[element.vote]) {
            count[element.vote] += 1
          } else {
            count[element.vote] = 1
          }
        }
      }
      let voteValues = []
      let voteCounts = []
      let keys = Object.keys(count)
      let values = Object.values(count)
      for (var i = 0; i < keys.length; i++) {
        voteValues.push(keys[i])
        voteCounts.push(values[i])
      }
      setChartData({
        labels: voteValues,
        datasets: [{ label: "Estimates", data: voteCounts, backgroundColor: ["#2a71d0", "#a9c25d", "#f69c53", "#15c921", "#e4ddf4", "#5bccf6", "#c5f82b", "#e33131", "#dddddd", "#70d979", "#785abf", "#feda6c"] }]
      })
      setResultsList(userList)
      setVoteValues(voteValues)
    }
  }, [room.gameState, userList])

  // ONCLICK REMOVE USER FROM CURRENTUSERS AND REDIRECT
  function leaveRoom(gameID, uid) {
    removeCurrentUser(gameID, uid)
    history.push(`/`)
  }

  // SET STATES TO DEAL CARDS TO PLAYERS
  function dealHand() {
    setRoomState(room, "voting")
    let keys = Object.keys(room.currentUsers)
    for (var i = 0; i < keys.length; i++) {
      setPlayerState(room.gameID, keys[i], "voting")
      setVote("voting", room.gameID, keys[i])
    }
  }

  // SET USER SELECTION IN DB ROOM DATA
  function selectCard(selection, gameID, playerID) {
    setVote(selection, gameID, playerID)
    setPlayerState(room.gameID, playerID, "voted")
  }

  // COUNT VOTES ONCHANGE OF ANY CURRENTUSERS VOTINGSTATE
  function countVotes() {
    let votes = 0
    let keys = Object.keys(room.currentUsers)
    let values = Object.values(room.currentUsers)
    if (keys.length > 0) {
      for (var i = 0; i < keys.length; i++) {
        if (values[i].vote !== undefined && values[i].playerState === "voted") {
          votes++
        } else if (values[i].vote === undefined || values[i].playerState === "ready") {
          votes++
        }
      }
      if (currentUser.uid === room.hostID && votes > 0 && votes === keys.length) {
        setRoomState(room, "voted")
      }
      setVotesWaiting(keys.length - votes)
    }
  }

  // SET GAME STATE SO RESULTS ARE REVEALED TO ALL PLAYERS
  function showHand() { setRoomState(room, "reveal") }

  // RESET ROOM/USER STATE FOR NEW GAME
  function reset() {
    setRoomState(room, "waiting")
    setRoomTimestamp(room.gameID)
    let keys = Object.keys(room.currentUsers)
    for (var i = 0; i < keys.length; i++) {
      setPlayerState(room.gameID, keys[i], "ready")
      setVote(null, room.gameID, keys[i])
    }
  }

  return (
    <Card className="gameWindow">
      <Card.Header>
        <Row>
          <Col className='leftAlign'>
            {room.roomName}
          </Col>
          <Col className='rightAlign'>
            <Button variant="outline-danger" className='smallBtn' onClick={() => leaveRoom(room.gameID, currentUser.uid)}>Leave Game</Button>
          </Col>
        </Row>
      </Card.Header>
      <Card.Body className="text-center gameWindowBody">

        {/* PLAYER GAMESTATE NOTICES */}

        {(room.gameState !== "waiting" && room.gameState !== "reveal" && user.playerState === "waiting" && room.currentUsers[room.hostID]) &&
          <Alert>
            <div className='loaderContainerTwo flexCenter'>
              <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
            <p className="mb-0">Waiting For Current Hand To End...</p>
          </Alert>
        }
        {(room.gameState === "waiting" && room.currentUsers[room.hostID]) &&
          <Alert>
            <div className='loaderContainerTwo flexCenter'>
              <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
            {(room.hostID === currentUser.uid) ?
              <p className="mb-0">Waiting For You To Deal...</p>
              :
              <p className="mb-0">Waiting For Host To Deal...</p>
            }
          </Alert>
        }
        {(user.playerState === "voted" && room.gameState === "voting" && room.currentUsers[room.hostID]) &&
          <>
            <Alert>
              <div className='loaderContainerTwo flexCenter'>
                <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
              </div>
              <p className="mb-0">Waiting For {votesWaiting} To Vote...</p>
              <br></br>
              <p className="mb-0 noticeSub"><strong>Your Estimate</strong></p>
              <Card className='gameCardOuterSelected'>
                <Card className='gameCardOuterTwo'>
                  <Card className='gameCard'>
                    <Card.Text className='cardValue flexCenter bold'>{user.vote}</Card.Text>
                  </Card>
                </Card>
              </Card>
            </Alert>
          </>
        }
        {(user.playerState === "voted" && room.gameState === "voted" && room.currentUsers[room.hostID]) &&
          <Alert>
            <div className='loaderContainerTwo flexCenter'>
              <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
            {(room.hostID === currentUser.uid) ?
              <p className="mb-0">Waiting For You To Reveal Cards...</p>
              :
              <p className="mb-0">Waiting For Host To Reveal Cards...</p>
            }
            <br></br>
            <p className="mb-0 noticeSub"><strong>Your Estimate</strong></p>
            <Card className='gameCardOuterSelected'>
              <Card className='gameCardOuterTwo'>
                <Card className='gameCard'>
                  <Card.Text className='cardValue flexCenter bold'>{user.vote}</Card.Text>
                </Card>
              </Card>
            </Card>
          </Alert>
        }

        {/* VOTING ELEMENTS */}

        {(room.gameState === "voting" && user.playerState === "voting" && room.currentUsers[room.hostID]) &&
          <>
            <Alert>
              <p className="mb-2">Select A Card To Submit An Estimate</p>
              <p className="mb-0 noticeSub">Waiting For Votes ({votesWaiting} Remaining)</p>
            </Alert>
            <div className='gameCards'>
              {room.gameType === "fib" ?
                <>
                  {fibValues.map((fibValue, i) => (
                    <React.Fragment key={i}>
                      <Card className='gameCardOuter'>
                        <Card className='gameCard' onClick={() => selectCard(fibValue, room.gameID, currentUser.uid)}>
                          <Card.Text className='cardValue flexCenter'>{fibValue}</Card.Text>
                        </Card>
                      </Card>
                    </React.Fragment>
                  ))}
                </>
                :
                <>
                  {teeValues.map((teeValue, i) => (
                    <React.Fragment key={i}>
                      <Card className='gameCardOuter'>
                        <Card className='gameCard' onClick={() => selectCard(teeValue, room.gameID, currentUser.uid)}>
                          <Card.Text className='cardValue flexCenter'>{teeValue}</Card.Text>
                        </Card>
                      </Card>
                    </React.Fragment>
                  ))}
                </>
              }
            </div>
          </>
        }

        {/* REVEAL RESULTS TO ALL PLAYERS */}

        {(room.gameState === "reveal" && room.currentUsers[room.hostID]) &&
          <div className='results'>
            <Alert>
              <p className="mb-2">Final Estimates</p>
              <p className="mb-0 noticeSub">Waiting For Next Hand...</p>
            </Alert>
            <br></br>
            <div className='gameCards'>
              <Row className='row results-row'>
                {(chartData) &&
                  <Col className='col results-col results-col-l'>
                    <Chart chartData={chartData} />
                  </Col>
                }
                {(voteValues.length > 0 && resultsList) &&
                  <Col className='col results-col results-col-r'>
                    <div className="results-table">
                      <Row className="row results-table-header-row">
                        <Col className="col results-table-col">Estimate</Col>
                        <Col className="col results-table-col">Users</Col>
                      </Row>
                      {voteValues.map((voteValue, voteIndex) => (
                        <Row className="row results-table-row" key={voteIndex}>
                          <Col className="col results-table-col results-table-col-left">{voteValue}</Col>
                          <Col className="col results-table-col results-table-col-right">
                            {resultsList.map((user, userIndex) => (
                              <React.Fragment key={userIndex}>
                                {user.vote === voteValue &&
                                  <>
                                    <img className='avatarResults' src={require(`../../images/avatars/${user.avatar}.png`)} alt="avatar" data-tip data-for={user.playerID + "1"}></img>
                                    <ReactTooltip id={user.playerID + "1"} className="tooltip" place="top" effect="solid"> {user.username} </ReactTooltip>
                                  </>
                                }
                              </React.Fragment>
                            ))}
                          </Col>
                        </Row>
                      ))}
                    </div>
                  </Col>
                }
              </Row>
            </div>
          </div>
        }

        {/* HOST HAS LEFT */}

        {(room.hostID && !room.currentUsers[room.hostID]) &&
          <Alert>
            <div className='mb-4 loaderContainerTwo flexCenter'>
              <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
            <p className="mb-2 danger-notice">THE HOST HAS LEFT THE GAME</p>
            <p className="mb-2">Waiting For Host To Join...</p>
          </Alert>
        }
      </Card.Body>

      {/* HOST CONTROLS */}

      {(room.hostID === currentUser.uid) &&
        <>
          <Card.Footer className="text-center hostControls">
            {(room.gameState === "waiting") &&
              <Button variant="primary" className='hostBtn' onClick={() => dealHand(room)}>Deal Cards</Button>
            }
            {((room.gameState === "voting" && user.playerState === "voting") || (room.gameState === "voting" && user.playerState === "voted")) &&
              <Button variant="primary" className='hostBtn' disabled>Waiting For {votesWaiting} Vote/s</Button>
            }
            {(room.gameState === "voted") &&
              <Button variant="primary" className='hostBtn' onClick={() => showHand()}>Reveal Estimates</Button>
            }
            {(room.gameState === "reveal") &&
              <Button variant="primary" className='hostBtn' onClick={() => reset()}>Start Another Game</Button>
            }
          </Card.Footer>
        </>
      }
    </Card>
  )
}
