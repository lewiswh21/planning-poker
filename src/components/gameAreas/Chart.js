// eslint-disable-next-line
import { Doughnut as ChartJS } from 'chart.js/auto'
import { Doughnut } from 'react-chartjs-2'

export const Chart = ({ chartData }) => {
    return (
        <>
            {(chartData.datasets) &&
                <div>
                    <Doughnut
                        data={chartData}
                        options={{
                            plugins: {
                                title: {
                                    display: false,
                                    text: "",
                                    fontFamily: "Montserrat"
                                },
                                legend: {
                                    display: true,
                                    position: "right"
                                },
                                tooltip: {
                                    padding: 6
                                }
                            }
                        }}
                    />
                </div>
            }
        </>
    )
}