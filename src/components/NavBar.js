import { Alert } from 'bootstrap'
import { useState } from 'react'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'

export default function NavBar() {
    const [error, setError] = useState("")
    const { currentUser, logout } = useAuth()
    const history = useHistory()

    async function handleLogout() {
        setError("")
        try {
            await logout()
            history.push("/login")
        } catch {
            setError("Failed to log out")
        }
    }

    return (
        <>
            <Navbar className='navBar' collapseOnSelect expand="lg" bg="primary" variant="dark">
                <Container>
                    <Navbar.Brand className='siteTitle' onClick={() => history.push(`/`)}>Studio Planning Poker</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto"></Nav>
                        <Nav>
                            {currentUser ?
                                <NavDropdown title={
                                    <>
                                        {currentUser.photoURL &&
                                            <img className='avatarNavbar' src={require(`../images/avatars/${currentUser.photoURL}.png`)} alt="avatar"></img>
                                        }
                                        {currentUser.email + " "}
                                    </>
                                } id="basic-nav-dropdown">
                                    <NavDropdown.Item className='navDropdownItem' href="/update-profile">Update Profile</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item className='navDropdownItem' onClick={handleLogout}>Logout</NavDropdown.Item>
                                </NavDropdown>
                                :
                                <>
                                    <Nav.Link href="/login">Login</Nav.Link>
                                    <Nav.Link href="/signup">Signup</Nav.Link>
                                </>
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            {error && <Alert variant="danger">{error}</Alert>}
        </>
    )
}
