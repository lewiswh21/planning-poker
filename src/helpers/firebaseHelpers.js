import { get, getDatabase, ref, child, set, onValue, update, remove } from "firebase/database"
import { getCurrentTimestamp } from "./gameHelpers"

//CHECK LAST DB CLEAN AND CLEAN RECORDS IF > 1 DAYS OLD (REMOVES OLD ROOMS AFTER 1 DAY AND OLD USERS AFTER 30 DAYS)
export function dBClean() {
    const now = getCurrentTimestamp()
    const db = ref(getDatabase())
    get(child(db, `clean/lastClean`)).then((snapshot) => {
        let dayDiff = (now - snapshot.val()) / (1000 * 3600 * 24)
        if (dayDiff > 1) {
            cleanRooms(now)
            cleanUsers(now)
            setLastClean(now)
        }
    }).catch((error) => {
        console.error(error)
    })
}

//SET TIME FOR LAST DBCLEAN
export function setLastClean(now) {
    const db = getDatabase()
    const stateRef = ref(db, 'clean')
    update(stateRef, { lastClean: now }
    ).catch((error) => {
        console.error(error)
    })
}

// ITERATE ROOMS TO CHECK IF LIVE
export function cleanRooms(now) {
    const db = getDatabase()
    const roomsRef = ref(db, 'rooms')
    onValue(roomsRef, (snapshot) => {
        if (snapshot.exists()) {
            const data = snapshot.val()
            let oldRooms = []
            let keys = Object.keys(data)
            for (var i = 0; i < keys.length; i++) {
                let k = keys[i]
                let dayDiff
                if (!data[k].startTime) {
                    oldRooms.push(k)
                }
                if (data[k].startTime) {
                    dayDiff = (now - data[k].startTime) / (1000 * 3600 * 24)
                    if (dayDiff > 1) {
                        oldRooms.push(k)
                    }
                }
            }
            removeOldRooms(oldRooms)
        }
    })
}

// REMOVE OLD ROOMS IF > 1 DAYS OLD
export function removeOldRooms(oldRooms) {
    for (var i = 0; i < oldRooms.length; i++) {
        const db = getDatabase()
        remove(ref(db, 'rooms/' + oldRooms[i])).catch((error) => {
            console.error(error)
        })
    }
}

// ITERATE USERS TO CHECK IF LIVE
export function cleanUsers(now) {
    const db = getDatabase()
    const usersRef = ref(db, 'users')
    onValue(usersRef, (snapshot) => {
        if (snapshot.exists()) {
            const data = snapshot.val()
            let oldUsers = []
            let keys = Object.keys(data)
            for (var i = 0; i < keys.length; i++) {
                let k = keys[i]
                let dayDiff
                if (!data[k].lastGame) {
                    oldUsers.push(k)
                }
                if (data[k].lastGame) {
                    dayDiff = (now - data[k].lastGame) / (1000 * 3600 * 24)
                    if (dayDiff > 30) {
                        oldUsers.push(k)
                    }
                }
            }
            removeOldUsers(oldUsers)
        }
    })
}

// REMOVE OLD USERS IF > 30 DAYS OLD
export function removeOldUsers(oldUsers) {
    for (var i = 0; i < oldUsers.length; i++) {
        const db = getDatabase()
        remove(ref(db, 'users/' + oldUsers[i])).catch((error) => {
            console.error(error)
        })
    }
}

//SET ROOM DATA IN DB
export function writeRoomData(userData) {
    const now = getCurrentTimestamp()
    const db = getDatabase()
    set(ref(db, 'rooms/' + userData.gameID), {
        hostID: userData.playerID,
        hostUsername: userData.username,
        roomName: userData.roomName,
        gameType: userData.gameType,
        validRoom: true,
        gameState: "waiting",
        startTime: now
    }).catch((error) => {
        console.error(error)
    })
}

// UPDATE ROOM TIMESTAMP
export function setRoomTimestamp(gameID) {
    const now = getCurrentTimestamp()
    const db = getDatabase()
    const stateRef = ref(db, 'rooms/' + gameID)
    update(stateRef, { startTime: now })
        .catch((error) => {
            console.error(error)
        })
}

// GET ROOM DATA FROM DB
export function readRoomData(gameID, setRoom) {
    const db = ref(getDatabase())
    get(child(db, `rooms/${gameID}`)).then((snapshot) => {
        if (snapshot.exists()) {
            setRoom({
                username: snapshot.val().hostUsername,
                gameID: gameID,
                hostID: snapshot.val().hostID,
                roomName: snapshot.val().roomName,
                gameType: snapshot.val().gameType,
                validRoom: true,
                gameState: snapshot.val().gameState,
                currentUsers: snapshot.val().currentUsers
            })
        } else {
            setRoom({ validRoom: false })
        }
    }).catch((error) => {
        console.error(error)
    })
}

// CHECK IF ROOM EXISTS / IS VALID ROOM ID
export function checkRoomExists(gameID, setGameExists) {
    const db = ref(getDatabase())
    get(child(db, `rooms/${gameID}`)).then((snapshot) => {
        if (snapshot.exists()) {
            setGameExists(true)
        } else {
            setGameExists(false)
        }
    }).catch((error) => {
        console.error(error)
    })
}

// SET USER DATA IN DB
export function writeUserData(userData) {
    const db = getDatabase()
    set(ref(db, 'users/' + userData.playerID), {
        username: userData.username,
        gameID: userData.gameID,
        roomName: userData.roomName,
        gameType: userData.gameType,
        playerState: "joining",
        lastGame: userData.lastGame
    }).catch((error) => {
        console.error(error)
    })
}

// GET USER DATA FROM DB
export function readUserData(playerID, setUser) {
    const db = ref(getDatabase())
    get(child(db, `users/${playerID}`)).then((snapshot) => {
        if (snapshot.exists()) {
            setUser({
                username: snapshot.val().username,
                gameID: snapshot.val().gameID,
                roomName: snapshot.val().roomName,
                gameType: snapshot.val().gameType,
                playerState: snapshot.val().playerState
            })
        } else {
            setUser({ playerState: "Invalid user!" })
        }
    }).catch((error) => {
        console.error(error)
    })
}

// SET CURRENT USERS IN ROOM
export function writeCurrentUsers(roomID, userData, email) {
    const db = getDatabase()
    set(ref(db, 'rooms/' + roomID + '/currentUsers/' + userData.playerID), {
        playerID: userData.playerID,
        username: userData.username,
        gameID: userData.gameID,
        email: email,
        playerState: "waiting",
        avatar: userData.avatar
    }).catch((error) => {
        console.error(error)
    })
}

// READ CURRENT USERS
export function readCurrentUsers(gameID, setUserList) {
    const db = getDatabase()
    const userRef = ref(db, 'rooms/' + gameID + '/currentUsers/')
    onValue(userRef, (snapshot) => {
        if (snapshot.exists()) {
            let tempUserList = []
            let players = snapshot.val()
            let keys = Object.keys(players)
            for (var i = 0; i < keys.length; i++) {
                let k = keys[i]
                tempUserList.push(players[k])
            }
            setUserList(tempUserList)
        }
    })
}

// REMOVE FROM CURRENT USERS
export function removeCurrentUser(gameID, userID) {
    const db = getDatabase()
    set(ref(db, 'rooms/' + gameID + '/currentUsers/' + userID), {
        username: null,
        gameID: null,
        email: null,
        playerState: null
    }).catch((error) => {
        console.error(error)
    })
}

// CHECK USER VALID ON ROOM ENTRY
export function checkUserValid(playerID, gameID, setValidUser) {
    const db = ref(getDatabase())
    get(child(db, 'rooms/' + gameID + '/currentUsers/' + playerID)).then((snapshot) => {
        if (snapshot.exists()) {
            setValidUser(true)
        } else {
            setValidUser(false)
        }
    }).catch((error) => {
        console.error(error)
    })
}

// UPDATE ROOM STATE
export function setRoomState(room, roomState) {
    const db = getDatabase()
    const stateRef = ref(db, 'rooms/' + room.gameID)
    update(stateRef, { gameState: roomState })
        .catch((error) => {
            console.error(error)
        })
}

// READ ROOM STATE
export function getRoomState(gameID, setRoom) {
    const db = getDatabase()
    const userRef = ref(db, 'rooms/' + gameID)
    onValue(userRef, (snapshot) => {
        setRoom({
            currentUsers: snapshot.val().currentUsers,
            gameID: gameID,
            hostID: snapshot.val().hostID,
            roomName: snapshot.val().roomName,
            gameType: snapshot.val().gameType,
            validRoom: snapshot.val().validRoom,
            gameState: snapshot.val().gameState
        })
    })
}

// UPDATE PLAYER STATE (ROOMS > CURRENTUSERS)
export function setPlayerState(gameID, userID, playerState) {
    const db = getDatabase()
    const stateRef = ref(db, 'rooms/' + gameID + '/currentUsers/' + userID)
    update(stateRef, { playerState: playerState })
        .catch((error) => {
            console.error(error)
        })
}

//READ PLAYER STATE
export function getPlayerState(gameID, setUser, uid) {
    const db = getDatabase()
    const userRef = ref(db, 'rooms/' + gameID + '/currentUsers/' + uid)
    onValue(userRef, (snapshot) => {
        setUser(snapshot.val())
    })
}

// SET VOTE VALUE
export function setVote(selection, gameID, playerID) {
    const db = getDatabase()
    const playerVoteRef = ref(db, 'rooms/' + gameID + '/currentUsers/' + playerID)
    update(playerVoteRef, { vote: selection })
        .catch((error) => {
            console.error(error)
        })
}

