import { Alert } from "react-bootstrap"

// HANDLE ERRORS WITH GENERIC ALERT
export function errorMessage(errorMessage) {
    return (
        <div className='errorMessage'>
            <Alert>
                <p className="mb-0 errorSub"><strong>{errorMessage}</strong></p>
                <br></br>
                <a href="/" className="btn btn-outline-danger">Go Back</a>
            </Alert>
        </div>
    )
}

//GENERATE UNIQUE ID'S
export function generateUniqueID() {
    return Date.now().toString(36) + Math.random().toString(36).substr(2)
}

//CURRENT TIMESTAMP
export function getCurrentTimestamp() {
    let timestamp = new Date()
    return timestamp.getTime()
}

//COPY BUTTON CLICKED
export async function copyButtonClicked() {
    navigator.clipboard.writeText(window.location.href)
}
