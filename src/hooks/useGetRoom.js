import { useContext, useEffect, useState } from 'react'
import { ref, get, getDatabase, child } from "firebase/database"
import RoomContext from '../contexts/RoomContext'

export const useGetRoom = (gameID) => {
    const [loading, setLoading] = useState(true)
    const { room, setRoom } = useContext(RoomContext)
    const [error, setError] = useState("")

    //SET LOADING STATUS & GET/SET ROOMCONTEXT
    useEffect(() => {
        setLoading(true)
        const db = ref(getDatabase())
        get(child(db, `rooms/${gameID}`)).then((snapshot) => {
            if (snapshot.exists()) {
                setRoom({
                    username: snapshot.val().hostUsername,
                    gameID: gameID,
                    hostID: snapshot.val().hostID,
                    roomName: snapshot.val().roomName,
                    gameType: snapshot.val().gameType,
                    validRoom: true,
                    gameState: snapshot.val().gameState,
                    currentUsers: snapshot.val().currentUsers
                })
            } else {
                setRoom({ validRoom: false })
            }
            setLoading(false)
        }).catch((error) => {
            setError(error)
            setLoading(false)
        })
    }, [gameID])  // eslint-disable-line react-hooks/exhaustive-deps

    return { loading, room, error }
}