import firebase from 'firebase/compat/app'
import "firebase/compat/auth"

const app = firebase.initializeApp({
  apiKey: "AIzaSyDcPq_jJdGKuEIn_nbAVD7n8ZMDRkBmVO8",
  authDomain: "studio-poker-planning.firebaseapp.com",
  databaseURL: "https://studio-poker-planning-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "studio-poker-planning",
  storageBucket: "studio-poker-planning.appspot.com",
  messagingSenderId: "24860334485",
  appId: "1:24860334485:web:b02edf5ed803e5f33dbe9c"
})

export const auth = app.auth()

export default app