import React, { useState, useMemo } from "react"
import { Container } from "react-bootstrap"
import { AuthProvider } from "./contexts/AuthContext"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import NavBar from "./components/NavBar"
import Dashboard from "./components/Dashboard"
import Signup from "./components/Signup"
import Login from "./components/Login"
import PrivateRoute from "./components/PrivateRoute"
import ForgotPassword from "./components/ForgotPassword"
import UpdateProfile from "./components/UpdateProfile"
import UserContext from "./contexts/UserContext"
import RoomContext from "./contexts/RoomContext"
import GameRoom from "./components/gameAreas/GameRoom"

export default function App() {
  // USESTATE - PRESERVES VALUES BETWEEN FUNCTION CALLS
  const [user, setUser] = useState({ username: "", gameID: "", playerID: "", roomName: "", gameType: "", playerState: "" })
  const [room, setRoom] = useState({ username: "", gameID: "", playerID: "", roomName: "", gameType: "", validRoom: false, gameState: "", currentUsers: [] })
  
  // USEMEMO - ONLY RECOMPUTES THE MEMOIZED VALUE WHEN ONE OF THE DEPENDENCIES CHANGE TO AVOID EXPENSIVE CALCULATIONS ON EVERY RENDER
  const userData = useMemo(() => ({ user, setUser }), [user, setUser])
  const roomData = useMemo(() => ({ room, setRoom }), [room, setRoom])

  // CONTEXT HOOKS (AUTH, USER, ROOM) - PASSES THE CONTEXT OBJECT/S ACROSS CHILD COMPONENTS (WOULD NORMALLY TRIGGER A RERENDER OF ALL CHILD VALUES IF NOT INSIDE OF USEMEMO)

  return (
    <>
      <Router>
        <AuthProvider>
          <UserContext.Provider value={userData}>
            <RoomContext.Provider value={roomData}>
              <NavBar />
              <Container className="d-flex align-items-center justify-content-center appContainer">
                <div className="w-100">
                  <Switch>
                    <PrivateRoute exact path="/" component={Dashboard} />
                    <PrivateRoute path="/update-profile" component={UpdateProfile} />
                    <PrivateRoute path="/game/:gameID" component={GameRoom} />
                    <Route path="/signup" component={Signup} />
                    <Route path="/login" component={Login} />
                    <Route path="/forgot-password" component={ForgotPassword} />
                    <Route exact path='*' component={Dashboard} />
                  </Switch>
                </div>
              </Container>
            </RoomContext.Provider>
          </UserContext.Provider>
        </AuthProvider>
      </Router>
    </>
  )
}